FROM ccr.ccs.tencentyun.com/library/golang:1.20 as builder

ARG REVISION

RUN mkdir -p /podinfo/

WORKDIR /podinfo

COPY . .
# 网络环境不好  构建失败 故添加此配置
ENV GOPROXY="https://goproxy.cn,direct"

RUN go mod download

# windows环境下构建linux可执行文件
ENV CGO_ENABLED=0
ENV GOOS=linux
ENV GOARCH=amd64

RUN CGO_ENABLED=0 go build -ldflags "-s -w \
    -X gitee.com/fpy-go/plugin/pkg/version.REVISION=${REVISION}" \
    -a -o bin/podinfo cmd/podinfo/*

# 网络环境不好  构建失败 故注释
#RUN CGO_ENABLED=0 go build -ldflags "-s -w \
#    -X gitee.com/fpy-go/plugin/pkg/version.REVISION=${REVISION}" \
#    -a -o bin/podcli cmd/podcli/*

FROM alpine:3.19

ARG BUILD_DATE
ARG VERSION
ARG REVISION

LABEL maintainer="stefanprodan"

RUN addgroup -S app \
    && adduser -S -G app app \
    && apk --no-cache add \
    ca-certificates curl netcat-openbsd

WORKDIR /home/app

COPY --from=builder /podinfo/bin/podinfo .
# 网络环境不好  构建失败 故注释
# COPY --from=builder /podinfo/bin/podcli /usr/local/bin/podcli
COPY ./ui ./ui
RUN chown -R app:app ./

USER app

CMD ["./podinfo"]
