package shared

import "gitee.com/fpy-go/plugin/pkg/extend/model"

// 除了标准的Protocol接口（自动注册）
// AddHelper 提供了额外的接口，用来自定义额外处理逻辑及调用时机
type AddHelper interface {
	Sum(string, string) (string, error)
}

// GRPC协议插件接口.
type Protocol interface {
	Put(key string, value string, a AddHelper) error
	Get(key string) (string, error)
	Info(args string) model.PluginInfo
	Encode(args string) model.JsonRes
	Decode(data model.DataReq) model.JsonRes
}
