package shared

import (
	"context"
	"fmt"
	"gitee.com/fpy-go/plugin/pkg/extend/model"
	"gitee.com/fpy-go/plugin/pkg/extend/module/grpc/proto"
	"github.com/gogf/gf/v2/encoding/gjson"
	hclog "github.com/hashicorp/go-hclog"
	plugin "github.com/hashicorp/go-plugin"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"google.golang.org/grpc"
	"strconv"
)

var Logger *zap.Logger

func init() {
	Logger, _ = initZap("info")
}

// GRPCClient is an implementation of KV that talks over RPC.
type GRPCClient struct {
	Broker *plugin.GRPCBroker
	Client proto.PluginClient
	Logger *zap.Logger
}

func (m *GRPCClient) Put(key string, value string, a AddHelper) error {
	addHelperServer := &GRPCAddHelperServer{Impl: a}

	var s *grpc.Server
	serverFunc := func(opts []grpc.ServerOption) *grpc.Server {
		s = grpc.NewServer(opts...)
		proto.RegisterAddHelperServer(s, addHelperServer)

		return s
	}

	// muti grpc的关键实现
	brokerID := m.Broker.NextId()
	go m.Broker.AcceptAndServe(brokerID, serverFunc)

	_, err := m.Client.Put(context.Background(), &proto.PutRequest{
		AddServer: brokerID,
		Key:       key,
		Value:     value,
	})

	s.Stop()
	return err
}

func (m *GRPCClient) Get(key string) (string, error) {
	resp, err := m.Client.Get(context.Background(), &proto.GetRequest{
		Key: key,
	})
	if err != nil {
		return "0", err
	}

	return resp.Value, nil
}

func (m *GRPCClient) Info(args string) model.PluginInfo {
	plugin := &model.PluginInfo{}
	resp, err := m.Client.Info(context.Background(), &proto.InfoRequest{
		Key: args,
	})
	if err != nil {
		return *plugin
	}
	err = gjson.DecodeTo(resp.Value, plugin)
	return *plugin
}
func (m *GRPCClient) Encode(args string) model.JsonRes {
	json := &model.JsonRes{}
	resp, err := m.Client.Encode(context.Background(), &proto.EncodeRequest{
		Key: args,
	})
	if err != nil {
		return model.JsonRes{}
	}

	gjson.DecodeTo(resp.Value, json)
	return *json
}
func (m *GRPCClient) Decode(data model.DataReq) model.JsonRes {
	json := &model.JsonRes{}
	st, err := gjson.EncodeString(data)
	resp, err := m.Client.Decode(context.Background(), &proto.DecodeRequest{
		Key: st,
	})
	if err != nil {
		return model.JsonRes{}
	}

	gjson.DecodeTo(resp.Value, json)
	return *json
}

// Here is the gRPC server that GRPCClient talks to.
type GRPCServer struct {
	// This is the real implementation
	Impl Protocol

	Broker *plugin.GRPCBroker
}

func (m *GRPCServer) Put(ctx context.Context, req *proto.PutRequest) (*proto.Empty, error) {
	conn, err := m.Broker.Dial(req.AddServer)
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	a := &GRPCAddHelperClient{proto.NewAddHelperClient(conn)}
	return &proto.Empty{}, m.Impl.Put(req.Key, req.Value, a)
}

func (m *GRPCServer) Get(ctx context.Context, req *proto.GetRequest) (*proto.GetResponse, error) {
	v, err := m.Impl.Get(req.Key)
	return &proto.GetResponse{Value: v}, err
}

func (m *GRPCServer) Info(ctx context.Context, req *proto.InfoRequest) (*proto.InfoResponse, error) {
	v := m.Impl.Info(req.Key)
	str, err := gjson.EncodeString(v)
	Logger.Info(fmt.Sprintf("GRPCServer Info：%s", str))

	return &proto.InfoResponse{Value: str}, err
}
func (m *GRPCServer) Encode(ctx context.Context, req *proto.EncodeRequest) (*proto.EncodeResponse, error) {
	v := m.Impl.Encode(req.Key)
	str, err := gjson.EncodeString(v)
	Logger.Info(fmt.Sprintf("GRPCServer Encode结果：%s", str))

	return &proto.EncodeResponse{Value: str}, err
}
func (m *GRPCServer) Decode(ctx context.Context, req *proto.DecodeRequest) (*proto.DecodeResponse, error) {
	json := &model.DataReq{}
	err := gjson.DecodeTo(req.Key, json)
	if err != nil {
		return &proto.DecodeResponse{}, nil
	}
	v := m.Impl.Decode(*json)
	str, err := gjson.EncodeString(v)
	Logger.Info(fmt.Sprintf("GRPCServer Decode结果：%s", str))
	return &proto.DecodeResponse{Value: ""}, nil
}

// GRPCClient is an implementation of KV that talks over RPC.
type GRPCAddHelperClient struct{ client proto.AddHelperClient }

func (m *GRPCAddHelperClient) Sum(a, b string) (string, error) {
	resp, err := m.client.Sum(context.Background(), &proto.SumRequest{
		A: a,
		B: b,
	})
	if err != nil {
		hclog.Default().Info("add.Sum", "client", "start", "err", err)
		return "0", err
	}
	return resp.R, err
}

type AddHelperImpl struct{}

func (*AddHelperImpl) Sum(a, b string) (string, error) {
	inta, erra := strconv.Atoi(a)
	intb, _ := strconv.Atoi(b)
	return strconv.Itoa(inta + intb), erra
}

// Here is the gRPC server that GRPCClient talks to.
type GRPCAddHelperServer struct {
	// This is the real implementation
	Impl AddHelper
}

func (m *GRPCAddHelperServer) Sum(ctx context.Context, req *proto.SumRequest) (resp *proto.SumResponse, err error) {
	r, err := m.Impl.Sum(req.A, req.B)
	if err != nil {
		return nil, err
	}
	return &proto.SumResponse{R: r}, err
}

func initZap(logLevel string) (*zap.Logger, error) {
	level := zap.NewAtomicLevelAt(zapcore.InfoLevel)
	switch logLevel {
	case "debug":
		level = zap.NewAtomicLevelAt(zapcore.DebugLevel)
	case "info":
		level = zap.NewAtomicLevelAt(zapcore.InfoLevel)
	case "warn":
		level = zap.NewAtomicLevelAt(zapcore.WarnLevel)
	case "error":
		level = zap.NewAtomicLevelAt(zapcore.ErrorLevel)
	case "fatal":
		level = zap.NewAtomicLevelAt(zapcore.FatalLevel)
	case "panic":
		level = zap.NewAtomicLevelAt(zapcore.PanicLevel)
	}

	zapEncoderConfig := zapcore.EncoderConfig{
		TimeKey:        "ts",
		LevelKey:       "level",
		NameKey:        "logger",
		CallerKey:      "caller",
		MessageKey:     "msg",
		StacktraceKey:  "stacktrace",
		LineEnding:     zapcore.DefaultLineEnding,
		EncodeLevel:    zapcore.LowercaseLevelEncoder,
		EncodeTime:     zapcore.ISO8601TimeEncoder,
		EncodeDuration: zapcore.SecondsDurationEncoder,
		EncodeCaller:   zapcore.ShortCallerEncoder,
	}

	zapConfig := zap.Config{
		Level:       level,
		Development: false,
		Sampling: &zap.SamplingConfig{
			Initial:    100,
			Thereafter: 100,
		},
		Encoding:         "json",
		EncoderConfig:    zapEncoderConfig,
		OutputPaths:      []string{"stderr"},
		ErrorOutputPaths: []string{"stderr"},
	}

	return zapConfig.Build()
}
