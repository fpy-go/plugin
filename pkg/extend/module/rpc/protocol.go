package rpc

import (
	"fmt"
	"gitee.com/fpy-go/plugin/pkg/extend/model"
	"net/rpc"
)

// ProtocolRPC 基于RPC实现
type ProtocolRPC struct {
	Client *rpc.Client
}

func (p *ProtocolRPC) Info(args string) model.PluginInfo {
	var resp model.PluginInfo
	err := p.Client.Call("Plugin.Info", args, &resp)
	if err != nil {
		//希望接口返回错误
		//这里没有太多其他选择。
		fmt.Println(err.Error())
		//panic(err)
	}
	return resp
}
func (p *ProtocolRPC) Encode(args string) model.JsonRes {
	var resp model.JsonRes
	err := p.Client.Call("Plugin.Encode", args, &resp)
	if err != nil {
		//希望接口返回错误
		//这里没有太多其他选择。
		//panic(err)
		resp.Code = 1
		resp.Message = "protocol.go Encode " + err.Error()
		return resp
	}

	return resp
}
func (p *ProtocolRPC) Decode(data model.DataReq) model.JsonRes {
	var resp model.JsonRes
	err := p.Client.Call("Plugin.Decode", data, &resp)
	if err != nil {
		//希望接口返回错误
		//这里没有太多其他选择。
		//panic(err)
		resp.Code = 1
		resp.Message = err.Error()
		return resp
	}
	return resp
}

// ProtocolRPCServer  GreeterRPC的RPC服务器，符合 net/rpc的要求
type ProtocolRPCServer struct {
	// 内嵌业务接口
	// 插件进程会将实现业务接口的对象赋值给Impl
	Impl Protocol
}

func (s *ProtocolRPCServer) Info(args string, resp *model.PluginInfo) error {
	*resp = s.Impl.Info(args)
	return nil
}
func (s *ProtocolRPCServer) Encode(args string, resp *model.JsonRes) error {
	*resp = s.Impl.Encode(args)
	return nil
}
func (s *ProtocolRPCServer) Decode(args model.DataReq, resp *model.JsonRes) error {
	*resp = s.Impl.Decode(args)
	return nil
}
