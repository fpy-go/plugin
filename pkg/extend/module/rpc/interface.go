package rpc

import "gitee.com/fpy-go/plugin/pkg/extend/model"

// Protocol RPC协议解析插件接口
type Protocol interface {
	Info(args string) model.PluginInfo
	Encode(args string) model.JsonRes
	Decode(data model.DataReq) model.JsonRes
}
