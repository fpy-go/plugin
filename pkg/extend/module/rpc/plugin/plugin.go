package plugin

import (
	"fmt"
	rpc2 "gitee.com/fpy-go/plugin/pkg/extend/module/rpc"
	gplugin "github.com/hashicorp/go-plugin"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"net/rpc"
	"os"
	"time"
)

var parentPid int

var Logger *zap.Logger

func init() {
	parentPid = os.Getppid()

	Logger, _ = initZap("info")
}

// ProtocolPlugin 插件接口实现
// 这有两种方法：服务器必须为此插件返回RPC服务器类型。我们为此构建了一个RPCServer。
// 客户端必须返回我们的接口的实现通过RPC客户端。我们为此返回RPC。
type ProtocolPlugin struct {
	Impl rpc2.Protocol
}

// Server 此方法由插件进程延迟调
func (t *ProtocolPlugin) Server(g *gplugin.MuxBroker) (interface{}, error) {
	checkParentAlive()
	return &rpc2.ProtocolRPCServer{
		Impl: t.Impl,
	}, nil
}

// Client 此方法由宿主进程调用
func (t *ProtocolPlugin) Client(b *gplugin.MuxBroker, c *rpc.Client) (interface{}, error) {
	return &rpc2.ProtocolRPC{Client: c}, nil
}

// checkParentAlive 检查父进程(也就是 client )是否退出，如果退出了，自己也需要退出。
func checkParentAlive() {
	go func() {
		for {
			if parentPid == 1 || os.Getppid() != parentPid {
				fmt.Println("parent no alive, exit")
				os.Exit(0)
			}
			_, err := os.FindProcess(parentPid)
			if err != nil {
				fmt.Println("parent no alive, exit")
				os.Exit(0)
			}

			time.Sleep(5 * time.Second)
		}
	}()
}

func initZap(logLevel string) (*zap.Logger, error) {
	level := zap.NewAtomicLevelAt(zapcore.InfoLevel)
	switch logLevel {
	case "debug":
		level = zap.NewAtomicLevelAt(zapcore.DebugLevel)
	case "info":
		level = zap.NewAtomicLevelAt(zapcore.InfoLevel)
	case "warn":
		level = zap.NewAtomicLevelAt(zapcore.WarnLevel)
	case "error":
		level = zap.NewAtomicLevelAt(zapcore.ErrorLevel)
	case "fatal":
		level = zap.NewAtomicLevelAt(zapcore.FatalLevel)
	case "panic":
		level = zap.NewAtomicLevelAt(zapcore.PanicLevel)
	}

	zapEncoderConfig := zapcore.EncoderConfig{
		TimeKey:        "ts",
		LevelKey:       "level",
		NameKey:        "logger",
		CallerKey:      "caller",
		MessageKey:     "msg",
		StacktraceKey:  "stacktrace",
		LineEnding:     zapcore.DefaultLineEnding,
		EncodeLevel:    zapcore.LowercaseLevelEncoder,
		EncodeTime:     zapcore.ISO8601TimeEncoder,
		EncodeDuration: zapcore.SecondsDurationEncoder,
		EncodeCaller:   zapcore.ShortCallerEncoder,
	}

	zapConfig := zap.Config{
		Level:       level,
		Development: false,
		Sampling: &zap.SamplingConfig{
			Initial:    100,
			Thereafter: 100,
		},
		Encoding:         "json",
		EncoderConfig:    zapEncoderConfig,
		OutputPaths:      []string{"stderr"},
		ErrorOutputPaths: []string{"stderr"},
	}

	return zapConfig.Build()
}
