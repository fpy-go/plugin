package model

import "encoding/gob"

func init() {
	gob.Register(EncodeModel{})
}

// EncodeModel 主结构
type (
	EncodeModel struct {
		Data  string `json:"data"`
		IsHex bool   `json:"isHex"`
	}
)
