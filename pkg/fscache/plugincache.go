package fscache

import (
	"errors"
	"gitee.com/fpy-go/plugin/pkg/extend"
	"log"
	"sync"

	"github.com/fsnotify/fsnotify"
)

// 基于云原生的场景，我们其实可以不把插件放到minio等网络存储中，直接本地存储更高效
// 原因在于：云原生下使用pv来共享目录，多个pod公用共一个pv，再加上路径监听，就可以实现多个pod的插件加载
type PluginWatcher struct {
	dir       string
	fswatcher *fsnotify.Watcher
	Cache     *sync.Map
}

// NewWatch creates a directory watcher and
// updates the cache when any file changes in that dir
func NewPluginWatcher(dir string) (*PluginWatcher, error) {
	if len(dir) < 1 {
		return nil, errors.New("directory is empty")
	}

	fw, err := fsnotify.NewWatcher()
	if err != nil {
		return nil, err
	}

	w := &PluginWatcher{
		dir:       dir,
		fswatcher: fw,
		Cache:     new(sync.Map),
	}

	log.Printf("plugin cache start watcher for %s", w.dir)
	err = w.fswatcher.Add(w.dir)
	if err != nil {
		return nil, err
	}

	// initial read
	err = w.init()
	if err != nil {
		return nil, err
	}

	return w, nil
}

// Watch watches for when kubelet updates the volume mount content
func (w *PluginWatcher) Watch() {
	go func() {
		for {
			select {
			// it can take up to a 2 minutes for kubelet to recreate the ..data symlink
			case event := <-w.fswatcher.Events:
				// 只处理创建：原因是因为如果处理更新，那么创建也会有更新事件
				// 那么就需要我们更新插件时，先删除在创建，才会加载最新的插件到manager
				if event.Op&fsnotify.Create == fsnotify.Create {
					// 这里处理plugin manager
					err := w.updateCache(event.Name)
					if err != nil {
						log.Printf("plugin cache load error %v", err)
					} else {
						log.Printf("plugin cache load %s", w.dir)
					}
				} else if event.Op&fsnotify.Write == fsnotify.Write {
					w.updateCache(event.Name)
				}
			case err := <-w.fswatcher.Errors:
				log.Printf("fswatcher %s error %v", w.dir, err)
			}
		}
	}()
}

// updateCache 加载pluginPath对应路径的插件到manager
func (w *PluginWatcher) updateCache(pluginPath string) error {
	_, err := extend.PluginLoad(pluginPath)
	return err
}

// init 初始化插件（可以从minio下载）
func (w *PluginWatcher) init() error {

	// 如果想从minio中下载插件，则先下载

	// 初始化插件manager
	if extend.GetProtocolPlugin() == nil {
		return nil
	}
	return nil
}
