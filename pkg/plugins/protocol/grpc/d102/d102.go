package main

import (
	"encoding/json"
	"fmt"
	"gitee.com/fpy-go/plugin/pkg/extend/consts/PluginHandleType"
	"gitee.com/fpy-go/plugin/pkg/extend/consts/PluginType"
	"gitee.com/fpy-go/plugin/pkg/extend/model"
	"gitee.com/fpy-go/plugin/pkg/extend/module/grpc"
	pluginimpl "gitee.com/fpy-go/plugin/pkg/extend/module/grpc/plugin"
	"gitee.com/fpy-go/plugin/pkg/extend/module/grpc/shared"
	"github.com/gogf/gf/v2/encoding/gjson"
	"github.com/gogf/gf/v2/util/guid"
	"github.com/hashicorp/go-hclog"
	"io/ioutil"
	"os"
	"strings"
	"time"

	"github.com/hashicorp/go-plugin"
)

// D102Plugin d102设备的插件实现
type D102Plugin struct {
	Logger hclog.Logger
}

type data struct {
	Value string
}

func (k *D102Plugin) Put(key string, value string, a shared.AddHelper) error {
	v, err := k.Get(key)
	if err != nil {
		Logger.Error(err.Error())
		return err
	}
	r, err := a.Sum(v, value)
	Logger.Error(fmt.Sprintf("sum后的结果：%s", r))
	if err != nil {
		Logger.Error(err.Error())
		return err
	}

	buf, err := json.Marshal(&data{r})
	if err != nil {
		Logger.Error(err.Error())
		return err
	}

	return ioutil.WriteFile("kv_"+key, buf, 0644)
}

func (k *D102Plugin) Get(key string) (string, error) {
	dataRaw, err := ioutil.ReadFile("kv_" + key)
	if err != nil {
		Logger.Error(err.Error())
		return "0", err
	}

	data := &data{}
	err = json.Unmarshal(dataRaw, data)
	if err != nil {
		Logger.Error(err.Error())
		return "0", err
	}

	return data.Value, nil
}

func (k *D102Plugin) Info(args string) (res model.PluginInfo) {
	defer func() {
		if err := recover(); err != nil {
			k.Logger.Error(fmt.Sprintf("插件处理异常：%s", err))
		}
	}()
	res.Name = "d102"
	res.Types = PluginType.Protocol
	res.HandleType = PluginHandleType.TcpServer
	res.Title = "D-102 v1设备协议-GRPC"
	res.Author = "Feng"
	res.Description = "对D-102插座设备进行数据采集v1"
	res.Version = "0.01"
	return res
}
func (k *D102Plugin) Encode(args string) (resp model.JsonRes) {
	defer func() {
		if err := recover(); err != nil {
			resp.Code = 500
			k.Logger.Error(fmt.Sprintf("插件处理异常：%s", err))
			resp.Message = fmt.Sprintf("插件处理异常：%s", err)
		}
	}()
	resp.Code = 200
	if !isJson(args) {
		k.Logger.Info(fmt.Sprintf("Encode接收到错误参数：%s,非json类型", args))
	}

	k.Logger.Info(fmt.Sprintf("Encode接收到参数：%s", args))

	jsonData, _ := gjson.DecodeToJson(args)

	var strMsgType = jsonData.Get("msgType").String()
	if strMsgType == "cloudReq" {
		// 平台主动发送的命令
		return encodeDataReq(jsonData)
	} else if strMsgType == "cloudRsp" {
		//平台收到设备的数据后的响应
		return encodeDataRsp(jsonData)
	}
	return resp
}
func (k *D102Plugin) Decode(data model.DataReq) (resp model.JsonRes) {
	defer func() {
		if err := recover(); err != nil {
			resp.Code = 500
			k.Logger.Error(fmt.Sprintf("插件处理异常：%s", err))
			resp.Message = fmt.Sprintf("插件处理异常：%s", err)
		}
	}()
	paras, _ := gjson.EncodeString(data)
	k.Logger.Info(fmt.Sprintf("Decode接收到参数：%s", paras))

	resp.Code = 200

	tmpData := strings.Split(string(data.Data), ";")
	var rd = make(map[string]model.Param)

	l := len(tmpData)
	nowTime := time.Now().Unix()
	if l > 7 {
		rd["HeadStr"] = model.Param{Value: tmpData[0], Time: nowTime}
		rd["DeviceID"] = model.Param{Value: tmpData[1], Time: nowTime}
		rd["Signal"] = model.Param{Value: tmpData[2], Time: nowTime}
		rd["Battery"] = model.Param{Value: tmpData[3], Time: nowTime}
		rd["Temperature"] = model.Param{Value: tmpData[4], Time: nowTime}
		rd["Humidity"] = model.Param{Value: tmpData[5], Time: nowTime}
		rd["Cycle"] = model.Param{Value: tmpData[6], Time: nowTime}
		//处理续传数据
		updateStr := make([]string, 0)
		for i := 7; i < l; i++ {
			updateStr = append(updateStr, tmpData[i])
		}
		rd["Update"] = model.Param{Value: updateStr, Time: nowTime}
	}

	resp.Code = 0

	resp.Data = model.SagooMqttModel{
		Id:            guid.S(),
		Version:       "1.0",
		Sys:           model.SysInfo{Ack: 0},
		Params:        rd,
		Method:        "thing.event.property.post",
		ModelFuncName: "upProperty",
	}
	return resp
}

func encodeDataRsp(json *gjson.Json) model.JsonRes {
	var resp model.JsonRes
	resp.Message = "encodeDataRsp"
	resp.Code = 200
	resp.Data = model.EncodeModel{
		IsHex: true,
		Data:  json.Get("data").String(),
	}
	return resp
}

func encodeDataReq(json *gjson.Json) model.JsonRes {
	var resp model.JsonRes
	resp.Message = "encodeDataReq"
	resp.Code = 200
	resp.Data = model.EncodeModel{
		IsHex: true,
		Data:  json.Get("data").String(),
	}
	return resp
}

func isJson(jsonStr string) bool {
	var res map[string]any
	if jsonStr == "" {
		return false
	}
	err := json.Unmarshal([]byte(jsonStr), &res)
	if err != nil {
		return false
	}
	return true
}

func main() {
	plugin.Serve(&plugin.ServeConfig{
		HandshakeConfig: grpc.HandshakeConfig,
		Plugins: map[string]plugin.Plugin{
			"grpc-d102.exe": &pluginimpl.ProtocolPlugin{Impl: &D102Plugin{
				Logger,
			}},
		},

		// A non-nil value here enables gRPC serving for this plugin...
		GRPCServer: plugin.DefaultGRPCServer,
	})
}

var Logger = hclog.New(&hclog.LoggerOptions{
	Level:      hclog.Trace,
	Output:     os.Stderr,
	JSONFormat: true,
})
