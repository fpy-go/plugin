package main

import (
	"fmt"
	"gitee.com/fpy-go/plugin/pkg/extend/consts/PluginHandleType"
	"gitee.com/fpy-go/plugin/pkg/extend/consts/PluginType"
	"gitee.com/fpy-go/plugin/pkg/extend/model"
	rpc2 "gitee.com/fpy-go/plugin/pkg/extend/module/rpc"
	"gitee.com/fpy-go/plugin/pkg/extend/module/rpc/plugin"
	"github.com/gogf/gf/v2/util/guid"
	"github.com/hashicorp/go-hclog"
	"os"
	"strings"
	"time"

	gplugin "github.com/hashicorp/go-plugin"
)

// ProtocolTgn52 实现
type ProtocolTgn52 struct {
	Logger hclog.Logger
}

func (p *ProtocolTgn52) Info(args string) model.PluginInfo {
	defer func() {
		if err := recover(); err != nil {
			p.Logger.Error(fmt.Sprintf("插件处理异常：%s", err))
		}
	}()
	var res = model.PluginInfo{}
	res.Name = "tgn52"
	res.Types = PluginType.Notice
	res.HandleType = PluginHandleType.TcpServer
	res.Title = "TG-N5 v2设备协议"
	res.Author = "Microrain"
	res.Description = "对TG-N5插座设备进行数据采集v2"
	res.Version = "0.01"
	return res
}

func (p *ProtocolTgn52) Encode(args string) (resp model.JsonRes) {
	defer func() {
		if err := recover(); err != nil {
			resp.Code = 500
			p.Logger.Error(fmt.Sprintf("插件处理异常：%s", err))
			resp.Message = fmt.Sprintf("插件处理异常：%s", err)
		}
	}()

	fmt.Println("接收到参数：", args)
	return resp
}

func (p *ProtocolTgn52) Decode(data model.DataReq) (resp model.JsonRes) {
	defer func() {
		if err := recover(); err != nil {
			resp.Code = 500
			p.Logger.Error(fmt.Sprintf("插件处理异常：%s", err))
			resp.Message = fmt.Sprintf("插件处理异常：%s", err)
		}
	}()

	resp.Code = 0

	tmpData := strings.Split(string(data.Data), ";")
	var rd = make(map[string]model.Param)

	l := len(tmpData)
	nowTime := time.Now().Unix()
	if l > 7 {
		rd["HeadStr"] = model.Param{Value: tmpData[0], Time: nowTime}
		rd["DeviceID"] = model.Param{Value: tmpData[1], Time: nowTime}
		rd["Signal"] = model.Param{Value: tmpData[2], Time: nowTime}
		rd["Battery"] = model.Param{Value: tmpData[3], Time: nowTime}
		rd["Temperature"] = model.Param{Value: tmpData[4], Time: nowTime}
		rd["Humidity"] = model.Param{Value: tmpData[5], Time: nowTime}
		rd["Cycle"] = model.Param{Value: tmpData[6], Time: nowTime}
		//处理续传数据
		updateStr := make([]string, 0)
		for i := 7; i < l; i++ {
			updateStr = append(updateStr, tmpData[i])
		}
		rd["Update"] = model.Param{Value: updateStr, Time: nowTime}
	}

	resp.Code = 0

	resp.Data = model.SagooMqttModel{
		Id:            guid.S(),
		Version:       "1.0",
		Sys:           model.SysInfo{Ack: 0},
		Params:        rd,
		Method:        "thing.event.property.post",
		ModelFuncName: "upProperty",
	}
	return resp
}

func main() {
	//调用plugin.Serve()启动侦听，并提供服务
	//ServeConfig 握手配置，插件进程和宿主机进程，都需要保持一致
	gplugin.Serve(&gplugin.ServeConfig{
		HandshakeConfig: rpc2.HandshakeConfig,
		Plugins:         pluginMap,
	})
}

var Logger = hclog.New(&hclog.LoggerOptions{
	Level:      hclog.Trace,
	Output:     os.Stderr,
	JSONFormat: true,
})

// 插件进程必须指定Impl，此处赋值为greeter对象
var pluginMap = map[string]gplugin.Plugin{
	"tgn52.exe": &plugin.ProtocolPlugin{
		Impl: &ProtocolTgn52{
			Logger: Logger,
		},
	},
}
