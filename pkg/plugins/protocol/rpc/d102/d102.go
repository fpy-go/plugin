package main

import (
	"encoding/json"
	"fmt"
	"gitee.com/fpy-go/plugin/pkg/extend/consts/PluginHandleType"
	"gitee.com/fpy-go/plugin/pkg/extend/consts/PluginType"
	"gitee.com/fpy-go/plugin/pkg/extend/model"
	rpc2 "gitee.com/fpy-go/plugin/pkg/extend/module/rpc"
	"gitee.com/fpy-go/plugin/pkg/extend/module/rpc/plugin"
	"github.com/gogf/gf/v2/encoding/gjson"
	"github.com/gogf/gf/v2/util/guid"
	"github.com/hashicorp/go-hclog"
	"os"
	"strings"
	"time"

	gplugin "github.com/hashicorp/go-plugin"
)

// ProtocolD102 实现
type ProtocolD102 struct {
	Logger hclog.Logger
}

func (p *ProtocolD102) Info(args string) (res model.PluginInfo) {
	defer func() {
		if err := recover(); err != nil {
			p.Logger.Error(fmt.Sprintf("插件处理异常：%s", err))
		}
	}()
	res.Name = "d102"
	res.Types = PluginType.Protocol
	res.HandleType = PluginHandleType.TcpServer
	res.Title = "D-102 v1设备协议"
	res.Author = "Feng"
	res.Description = "对D-102插座设备进行数据采集v1"
	res.Version = "0.01"
	return res
}

func (p *ProtocolD102) Encode(args string) (resp model.JsonRes) {
	defer func() {
		if err := recover(); err != nil {
			resp.Code = 500
			p.Logger.Error(fmt.Sprintf("插件处理异常：%s", err))
			resp.Message = fmt.Sprintf("插件处理异常：%s", err)
		}
	}()
	if !isJson(args) {
		p.Logger.Info(fmt.Sprintf("Encode接收到错误参数：%s,非json类型", args))
	}
	var s hclog.Logger = nil
	s.Error("")
	p.Logger.Info(fmt.Sprintf("Encode接收到参数：%s", args))

	jsonData, _ := gjson.DecodeToJson(args)

	var strMsgType = jsonData.Get("msgType").String()
	if strMsgType == "cloudReq" {
		// 平台主动发送的命令
		return encodeDataReq(jsonData)
	} else if strMsgType == "cloudRsp" {
		//平台收到设备的数据后的响应
		return encodeDataRsp(jsonData)
	}
	return resp
}

func (p *ProtocolD102) Decode(data model.DataReq) (resp model.JsonRes) {
	defer func() {
		if err := recover(); err != nil {
			resp.Code = 500
			p.Logger.Error(fmt.Sprintf("插件处理异常：%s", err))
			resp.Message = fmt.Sprintf("插件处理异常：%s", err)
		}
	}()
	resp.Code = 0

	tmpData := strings.Split(string(data.Data), ";")
	var rd = make(map[string]model.Param)

	l := len(tmpData)
	nowTime := time.Now().Unix()
	if l > 7 {
		rd["HeadStr"] = model.Param{Value: tmpData[0], Time: nowTime}
		rd["DeviceID"] = model.Param{Value: tmpData[1], Time: nowTime}
		rd["Signal"] = model.Param{Value: tmpData[2], Time: nowTime}
		rd["Battery"] = model.Param{Value: tmpData[3], Time: nowTime}
		rd["Temperature"] = model.Param{Value: tmpData[4], Time: nowTime}
		rd["Humidity"] = model.Param{Value: tmpData[5], Time: nowTime}
		rd["Cycle"] = model.Param{Value: tmpData[6], Time: nowTime}
		//处理续传数据
		updateStr := make([]string, 0)
		for i := 7; i < l; i++ {
			updateStr = append(updateStr, tmpData[i])
		}
		rd["Update"] = model.Param{Value: updateStr, Time: nowTime}
	}

	resp.Code = 0

	resp.Data = model.SagooMqttModel{
		Id:            guid.S(),
		Version:       "1.0",
		Sys:           model.SysInfo{Ack: 0},
		Params:        rd,
		Method:        "thing.event.property.post",
		ModelFuncName: "upProperty",
	}
	return resp
}

func encodeDataRsp(json *gjson.Json) model.JsonRes {
	var resp model.JsonRes
	resp.Message = "encodeDataRsp"
	resp.Code = 200
	resp.Data = model.EncodeModel{
		IsHex: true,
		Data:  json.Get("data").String(),
	}
	return resp
}

func encodeDataReq(json *gjson.Json) model.JsonRes {
	var resp model.JsonRes
	resp.Message = "encodeDataReq"
	resp.Code = 200
	resp.Data = model.EncodeModel{
		IsHex: true,
		Data:  json.Get("data").String(),
	}
	return resp
}

func isJson(jsonStr string) bool {
	var res map[string]any
	if jsonStr == "" {
		return false
	}
	err := json.Unmarshal([]byte(jsonStr), &res)
	if err != nil {
		return false
	}
	return true
}

func main() {
	//调用plugin.Serve()启动侦听，并提供服务
	//ServeConfig 握手配置，插件进程和宿主机进程，都需要保持一致
	gplugin.Serve(&gplugin.ServeConfig{
		HandshakeConfig: rpc2.HandshakeConfig,
		Plugins:         pluginMap,
	})
}

var Logger = hclog.New(&hclog.LoggerOptions{
	Level:      hclog.Trace,
	Output:     os.Stderr,
	JSONFormat: true,
})

// 插件进程必须指定Impl，此处赋值为greeter对象
var pluginMap = map[string]gplugin.Plugin{
	"rpc-d102.exe": &plugin.ProtocolPlugin{
		Impl: &ProtocolD102{Logger},
	},
}
