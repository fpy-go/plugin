package api

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"gitee.com/fpy-go/plugin/pkg/extend"
	"gitee.com/fpy-go/plugin/pkg/extend/model"
	"github.com/gogf/gf/v2/encoding/gjson"
	"github.com/gogf/gf/v2/util/gconv"
	"io"
	"net/http"
	"net/http/httptrace"
	"sync"

	"gitee.com/fpy-go/plugin/pkg/version"
	"go.opentelemetry.io/contrib/instrumentation/net/http/httptrace/otelhttptrace"
	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
	"go.uber.org/zap"
)

// Echo godoc
// @Summary Echo
// @Description forwards the call to the backend service and echos the posted content
// @Tags HTTP API
// @Accept json
// @Produce json
// @Router /api/echo [post]
// @Success 202 {object} api.MapResponse
func (s *Server) echoHandler(w http.ResponseWriter, r *http.Request) {
	ctx, span := s.tracer.Start(r.Context(), "echoHandler")
	defer span.End()

	s.test()

	body, err := io.ReadAll(r.Body)
	if err != nil {
		s.logger.Error("reading the request body failed", zap.Error(err))
		s.ErrorResponse(w, r, span, "invalid request body", http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	client := http.Client{Transport: otelhttp.NewTransport(http.DefaultTransport)}

	if len(s.config.BackendURL) > 0 {
		result := make([]string, len(s.config.BackendURL))
		var wg sync.WaitGroup
		wg.Add(len(s.config.BackendURL))
		for i, b := range s.config.BackendURL {
			go func(index int, backend string) {
				defer wg.Done()

				ctx = httptrace.WithClientTrace(ctx, otelhttptrace.NewClientTrace(ctx))
				ctx, cancel := context.WithTimeout(ctx, s.config.HttpClientTimeout)
				defer cancel()

				backendReq, err := http.NewRequestWithContext(ctx, "POST", backend, bytes.NewReader(body))
				if err != nil {
					s.logger.Error("backend call failed", zap.Error(err), zap.String("url", backend))
					return
				}

				// forward headers
				copyTracingHeaders(r, backendReq)

				backendReq.Header.Set("X-API-Version", version.VERSION)
				backendReq.Header.Set("X-API-Revision", version.REVISION)

				// call backend
				resp, err := client.Do(backendReq)
				if err != nil {
					s.logger.Error("backend call failed", zap.Error(err), zap.String("url", backend))
					result[index] = fmt.Sprintf("backend %v call failed %v", backend, err)
					return
				}
				defer resp.Body.Close()

				// copy error status from backend and exit
				if resp.StatusCode >= 400 {
					s.logger.Error("backend call failed", zap.Int("status", resp.StatusCode), zap.String("url", backend))
					result[index] = fmt.Sprintf("backend %v response status code %v", backend, resp.StatusCode)
					return
				}

				// forward the received body
				rbody, err := io.ReadAll(resp.Body)
				if err != nil {
					s.logger.Error(
						"reading the backend request body failed",
						zap.Error(err),
						zap.String("url", backend))
					result[index] = fmt.Sprintf("backend %v call failed %v", backend, err)
					return
				}

				s.logger.Debug(
					"payload received from backend",
					zap.String("response", string(rbody)),
					zap.String("url", backend))

				result[index] = string(rbody)
			}(i, b)
		}
		wg.Wait()

		w.Header().Set("X-Color", s.config.UIColor)
		s.JSONResponse(w, r, result)

	} else {
		w.Header().Set("X-Color", s.config.UIColor)
		w.WriteHeader(http.StatusAccepted)
		w.Write(body)
	}
}

func copyTracingHeaders(from *http.Request, to *http.Request) {
	headers := []string{
		"x-request-id",
		"x-b3-traceid",
		"x-b3-spanid",
		"x-b3-parentspanid",
		"x-b3-sampled",
		"x-b3-flags",
		"x-ot-span-context",
	}

	for i := range headers {
		headerValue := from.Header.Get(headers[i])
		if len(headerValue) > 0 {
			to.Header.Set(headers[i], headerValue)
		}
	}
}

func (s *Server) test() {
	//pluginData, err := extend.GetProtocolPlugin().GetProtocolUnpackData("rpc-tgn52.exe", gconv.Bytes("NB1;1234567;1;2;+25.5;00;030;+21;+22"))
	//s.logger.Debug(fmt.Sprintf("GetProtocolUnpackData", pluginData))

	var rd = model.DataReq{}
	rd.Data = gconv.Bytes(111)
	//pluginData, err = extend.GetProtocolPlugin().GetProtocolUnpackData1("tgn52.exe", rd)
	//s.logger.Debug(fmt.Sprintf("GetProtocolUnpackData", pluginData))

	pluginData, err := extend.GetProtocolPlugin().GetProtocolUnpackData("rpc-d102.exe", gconv.Bytes("NB1;1234567;1;2;+25.5;00;030;+21;+22"))
	pluginData1, _ := gjson.EncodeString(pluginData)
	s.logger.Info(fmt.Sprintf("d102", pluginData1))

	res := make(map[string]any)

	res["data"] = gconv.Bytes(111)
	res["msgType"] = "cloudReq"
	rdS, _ := gjson.EncodeString(res)
	pluginData, err = extend.GetProtocolPlugin().GetProtocolUnpackData1("rpc-d102.exe", rdS)
	pluginData1, _ = gjson.EncodeString(pluginData)
	s.logger.Info(fmt.Sprintf("d102", pluginData1))

	var infoReq = model.InfoReq{}
	infoReq.Data = gconv.Bytes(111)
	json, err := Json2Map("{\"title\":\"json在线解析（简版） -JSON在线解析\"}")
	if err != nil {
		s.logger.Error(fmt.Sprintf("错误或者正确:%s", json))
		return
	}

	pluginData2, err := extend.GetProtocolPlugin().GetProtocolInfo("rpc-d102.exe", "1111")
	pluginData1, _ = gjson.EncodeString(pluginData2)
	s.logger.Info(fmt.Sprintf("d102", pluginData1))

	//pluginData2, err := extend.GetProtocolPlugin().GetProtocolUnpackData("grpc-d102.exe", gconv.Bytes("NB1;1234567;1;2;+25.5;00;030;+21;+22"))
	//pluginData1, _ := gjson.EncodeString(pluginData2)
	//s.logger.Info(fmt.Sprintf("d102", pluginData1))
	//s.logger.Info(fmt.Sprintf("d102", err))
	//
	//res := make(map[string]any)
	//
	//res["data"] = gconv.Bytes(111)
	//res["msgType"] = "cloudReq"
	//rdS, _ := gjson.EncodeString(res)
	//pluginData2, err1 := extend.GetProtocolPlugin().GetProtocolUnpackData1("grpc-d102.exe", rdS)
	//pluginData1, err1 = gjson.EncodeString(pluginData2)
	//s.logger.Info(fmt.Sprintf("grpc-d102", pluginData1))
	//s.logger.Info(fmt.Sprintf("grpc-d102", err1))
}

func Json2Map(jsonStr string) (map[string]any, error) {
	var res map[string]any
	if jsonStr == "" {
		return res, errors.New("参数为空")
	}
	err := json.Unmarshal([]byte(jsonStr), &res)
	return res, err
}
