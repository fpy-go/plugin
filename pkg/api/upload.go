package api

import (
	"fmt"
	"gitee.com/fpy-go/plugin/pkg/config"
	"gitee.com/fpy-go/plugin/pkg/global"
	"github.com/PandaXGO/PandaKit/biz"
	"github.com/PandaXGO/PandaKit/oss"
	"github.com/PandaXGO/PandaKit/restfulx"
	"net/http"
	"os"
	"path"
	"time"
)

type UploadApi struct{}

const filePath = "uploads/file"

// oss godoc
// @Summary Echo
// @Description forwards the call to the backend service and oss the posted content
// @Tags HTTP API
// @Accept json
// @Produce json
// @Router /api/echo [post]
// @Success 202 {object} api.MapResponse
func (s *Server) uplaodToOss(w http.ResponseWriter, r *http.Request) {
	// 如何换成其他包装了otel的client
	//ctx, span := s.tracer.Start(r.Context(), "echoHandler")
	//defer span.End()
	//
	//body, err := io.ReadAll(r.Body)
	//if err != nil {
	//	s.logger.Error("reading the request body failed", zap.Error(err))
	//	s.ErrorResponse(w, r, span, "invalid request body", http.StatusBadRequest)
	//	return
	//}
	//defer r.Body.Close()

	// client := http.Client{Transport: otelhttp.NewTransport(http.DefaultTransport)}

	_, handler, _ := r.FormFile("file")
	yunFileTmpPath := "uploads/" + time.Now().Format("2006-01-02") + "/" + handler.Filename
	// 读取本地文件。
	f, openError := handler.Open()
	biz.ErrIsNil(openError, "function file.Open() Failed")
	config := global.Conf.Oss
	biz.ErrIsNil(NewOss(*config).PutObj(yunFileTmpPath, f), "上传OSS失败")

	s.JSONResponse(w, r, fmt.Sprintf("http://%s/%s/%s", config.Endpoint, config.BucketName, yunFileTmpPath))
}

func (s *Server) getFromOss(w http.ResponseWriter, r *http.Request) {
	// 如何换成其他包装了otel的client
	//ctx, span := s.tracer.Start(r.Context(), "echoHandler")
	//defer span.End()
	//
	//body, err := io.ReadAll(r.Body)
	//if err != nil {
	//	s.logger.Error("reading the request body failed", zap.Error(err))
	//	s.ErrorResponse(w, r, span, "invalid request body", http.StatusBadRequest)
	//	return
	//}
	//defer r.Body.Close()

	// client := http.Client{Transport: otelhttp.NewTransport(http.DefaultTransport)}

	config := global.Conf.Oss
	biz.ErrIsNil(NewOss(*config).Get("1654155731536.jpg", "./test.jpg"), "下载OSS失败")
}

func (up *UploadApi) GetImage(rc *restfulx.ReqCtx) {
	actual := path.Join(filePath, restfulx.PathParam(rc, "subpath"))
	http.ServeFile(
		rc.Response.ResponseWriter,
		rc.Request.Request,
		actual)
}

func (up *UploadApi) DeleteImage(rc *restfulx.ReqCtx) {
	fileName := restfulx.QueryParam(rc, "fileName")
	biz.NotEmpty(fileName, "请传要删除的图片名")
	err := os.Remove(fmt.Sprintf("%s/%s", filePath, fileName))
	biz.ErrIsNil(err, "文件删除失败")
}

func NewOss(ens config.Oss) oss.Driver {
	return oss.NewMiniOss(oss.MiniOConfig{
		BucketName:      ens.BucketName,
		Endpoint:        ens.Endpoint,
		AccessKeyID:     ens.AccessKey,
		SecretAccessKey: ens.SecretKey,
		UseSSL:          ens.UseSSL,
		Location:        "cn-north-1",
	})
}
